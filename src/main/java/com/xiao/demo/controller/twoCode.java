package com.xiao.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiao.demo.utils.JsonUtil;
import com.xiao.demo.utils.QrCodeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/test")
public class twoCode {


    @Value("${wechat.appid}")
    public  String appid;

    @Value("${wechat.secret}")
    public  String secret;


    /**
     * 接收二维码
     * @param request
     * @return
     * @throws IOException
     */
    @RequestMapping(value="/twoCode",method=RequestMethod.POST,produces="text/html;charset=utf-8")
    @ResponseBody
    public Object twoCode(HttpServletRequest request) throws IOException {
        JSONObject data=new JSONObject();
        String accessToken = QrCodeUtil.getToken(appid,secret);
        System.out.println("accessToken;"+accessToken);
        String twoCodeUrl = null;
        try {
            twoCodeUrl = QrCodeUtil.getCodePic("root","pages/login/login","root",accessToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(twoCodeUrl);
        data.put("twoCodeUrl", twoCodeUrl);
        return data;

    }

    @Test
    public void  get() throws Exception {//wx6708bdc8fb872786      895f054ff6ea6ef33cdd9441ccf2ab3f
        //String token = QrCodeUtil.getToken("wxec0c447ca266a4d3", "2179bcec7f968497cc408b142c46299b");
        //String token = QrCodeUtil.getToken("wx6708bdc8fb872786", "895f054ff6ea6ef33cdd9441ccf2ab3f");

        //String token = QrCodeUtil.getToken("wx77511957a9a01f4a", "23521113e8ba14dcc4ecab91f592e0ed");

        String token = QrCodeUtil.getToken("wxed63190088dd07c3", "ce789e78d483f80cd189e7dc2d70509a");

        QrCodeUtil.getCodePic("root","pages/login/login","root",token);
    }

    @Test
    public void get2() {
        String json ="['37','32','29','24']";
        List list = JsonUtil.getList(json, Integer.class);
        String s = "";
    }

    @Test
    public void get3() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD HH:mm:SS");
        Date add_time =sdf.parse("2018-12-05 15:30:00");
        Date now_time = new Date();//当前时间

        long end_long = add_time.getTime() + (30 * 60000);
        long l = end_long - now_time.getTime();
        long day = l / (24 * 60 * 60 * 1000);
        long hour = (l / (60 * 60 * 1000) - day * 24);
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        System.out.println("l;"+l);
        System.out.println("l/1000;"+l/1000);
        System.out.println("min;"+min);
       // System.out.println("l/1000;"+l/1000);
    }

    @Test
    public void get4(){
        BigDecimal a = null;
        BigDecimal d = new BigDecimal(766);
        a = d.add(new BigDecimal(398));
        System.out.println(a.doubleValue());
    }


    @Test
    public void get5(){
        Integer junDou = 0;
        Integer junDouOne = 1;
        Integer junDouTwo = 0;
        Integer junDouThree = 0;

        junDou=junDouOne+junDouTwo+junDouThree;

        if(junDou!=0){
            System.out.println("+++++++++++");
        }
        System.out.println("------------");
    }

}
