package com.xiao.demo.utils;

import com.alibaba.fastjson.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 二维码生成
 * 
 * @author Administrator
 *
 */
public class QrCodeUtil_new {

    /**
     * 二维码保存地址
     * 
     */
    public static String smallCodeSave = "D:\\wcf/";

    /*
     * 获取 token
     * 普通的 get 可获 token
     */
    public static String getToken() {
        try {

            Map<String, String> map = new LinkedHashMap<String, String>();

            // private static String APP_APPID = "wx6708bdc8fb872786";
            // private static String APP_SECRET = "895f054ff6ea6ef33cdd9441ccf2ab3f";

            map.put("grant_type", "client_credential");
            // map.put("appid", "wxa0287e8ace72395c");// 改成自己的appid
            // map.put("secret", "dda4b4ac7fdb368fa0386db522204f02");

            //map.put("appid", "wx6708bdc8fb872786");// 改成自己的appid
            //map.put("secret", "895f054ff6ea6ef33cdd9441ccf2ab3f");

            //新益生轻
            map.put("appid", "wxed63190088dd07c3");// 改成自己的appid
            map.put("secret", "ce789e78d483f80cd189e7dc2d70509a");



            String rt = UrlUtil.sendPost("https://api.weixin.qq.com/cgi-bin/token", map);

            System.out.println("what is:" + rt);
            JSONObject json = JSONObject.parseObject(rt);

            if (json.getString("access_token") != null || json.getString("access_token") != "") {
                return json.getString("access_token");
            } else {
                return null;
            }
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }

    }

    /**
     *
     * @param scene 传递参数
     * @param page 二维码跳转页面
     * @param membersAccount 会员id
     * @param access_token
     * @return 二维码文件名
     * @throws Exception
     */
    public static String getCodePic(String scene, String page, String membersAccount, String access_token,
        String savePath) throws Exception {

        // String =request.getSession().getServletContext().getRealPath("/");
        // String path = savePath + membersAccount;
        File servicePath = new File(savePath);
        if (!servicePath.exists() && !servicePath.isDirectory()) {
            servicePath.mkdir();
        }

        String path = savePath + membersAccount;
        File f = new File(path);
        if (!f.exists() && !f.isDirectory()) {
            f.mkdir();
        }
        try {
            URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + access_token);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");// 提交模式
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            paramJson.put("scene", scene);
            paramJson.put("page", page);
            paramJson.put("width", 500);
            paramJson.put("auto_color", true);

            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            // 开始获取数据
            BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());
            String fileName = path + "/" + membersAccount + ".png";

            OutputStream os = new FileOutputStream(new File(fileName));
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1) {
                os.write(arr, 0, len);
                os.flush();
            }
            os.close();
            String[] str = membersAccount.split("cn");
            // String newPath = "E:\\var\\data\\iweichuangfu\\images\\code\\" + membersAccount + "/" + fileName;
            coverImage(fileName, savePath + "ditu1.png", 0, 0, 350, 350, str[1], fileName);// 二维码中间的那张LOGO底色
            // new File(fileName).delete();


            if(getImgWidth(fileName)){
                getCodePic(scene, page, membersAccount, access_token,
                        savePath);
            }
            return membersAccount + "/" + fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static BufferedImage coverImage(String baseFilePath, String coverFilePath, int x, int y, int width,
        int height, String info, String url) throws Exception {

        BufferedImage destImage = null;
        int allw = 0, allh = 0;

        File baseFile = new File(baseFilePath);// 底图
        BufferedImage buffImg = ImageIO.read(baseFile);

        int srcImgWidth = buffImg.getWidth(null);// 获取图片的宽
        int srcImgHeight = buffImg.getHeight(null);// 获取图片的高

        File coverFile = new File(coverFilePath); // 覆盖层
        BufferedImage coverImg = ImageIO.read(coverFile);

        allh = srcImgHeight + coverImg.getHeight(null);

        destImage = new BufferedImage(coverImg.getWidth(), coverImg.getHeight(), BufferedImage.TYPE_INT_RGB);

        Graphics2D g = coverImg.createGraphics();
        g.setFont(new Font("黑体", Font.PLAIN, 38)); // 设置字体
        // g.setBackground(new Color(0,0,100));
        g.setColor(new Color(0, 0, 0));

        // 设置水印的坐标
        x = 0;
        y = 0;
        System.out.println("x:" + x);
        System.out.println("y:" + y);
        int strWidth = g.getFontMetrics().stringWidth(info);

        System.out.println("strWidth :" + strWidth);

        // g.drawString(info, 125-strWidth/2, 0);
        g.drawString(info, (coverImg.getWidth() - strWidth) / 2, coverImg.getHeight()-50);
        g.dispose();

        buffImg = coverImage(destImage, buffImg, coverImg, x, y, info);

        FileOutputStream outImgStream = new FileOutputStream(url);
        ImageIO.write(buffImg, "png", outImgStream);
        System.out.println("添加水印完成");
        outImgStream.flush();
        outImgStream.close();

        return buffImg;
    }

    public static BufferedImage coverImage(BufferedImage baseBufferedImage, BufferedImage BufferedImage,
        BufferedImage coverBufferedImage, int x, int y, String info) throws Exception {

        // 创建Graphics2D对象，用在底图对象上绘图
        Graphics2D g2d = baseBufferedImage.createGraphics();

        // 绘制
        // g2d.drawImage(coverBufferedImage, 170, 170, 300, 300, null);
        g2d.drawImage(coverBufferedImage, 0, 0, coverBufferedImage.getWidth(), coverBufferedImage.getHeight(), null);
        g2d.drawImage(BufferedImage, 100, 50, BufferedImage.getWidth(), BufferedImage.getHeight(), null);

        g2d.dispose();// 释放图形上下文使用的系统资源
        return baseBufferedImage;
    }

    public static Boolean getImgWidth(String url) throws Exception {
        Boolean b;
        File coverFile = new File(url); // 覆盖层
        BufferedImage coverImg = ImageIO.read(coverFile);
        int srcImgWidth = coverImg.getWidth(null);// 获取图片的宽
        if(srcImgWidth>0){
            b = false;
        }else{
            b = true;
        }
        return b;
    }


}
