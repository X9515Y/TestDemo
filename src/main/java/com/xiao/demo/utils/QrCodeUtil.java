package com.xiao.demo.utils;


import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

public class QrCodeUtil {



    /**
     * 二维码保存地址
     */
    public static String smallCodeSave = "E:\\var\\data\\iweichuangfu\\images\\code\\";



    /*
     * 获取 token
     * 普通的 get 可获 token
     */
    public  static String getToken(String appid,String secret) {


        try {


            Map<String, String> map = new LinkedHashMap<String, String>();
            map.put("grant_type", "client_credential");
            map.put("appid", "wxa0287e8ace72395c");//改成自己的appid
            map.put("secret", "dda4b4ac7fdb368fa0386db522204f02");


            map.put("appid",appid);
            map.put("secret", secret);

            String rt = UrlUtil.sendPost("https://api.weixin.qq.com/cgi-bin/token", map);

            System.out.println("what is:"+rt);
            JSONObject json = JSONObject.parseObject(rt);

            if (json.getString("access_token") != null || json.getString("access_token") != "") {
                return json.getString("access_token");
            } else {
                return null;
            }
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }

    }

    /*
     * 获取 二维码图片
     *
     * 测试
     *
     */
   /* public static String getminiqrQr( String accessToken,HttpServletRequest request) {
      //  String p=request.getSession().getServletContext().getRealPath("/");
        String p = "C:/Users/Administrator/Desktop/code";
        String codeUrl=p+"/twoCode.png";
        String twoCodeUrl="twoCode.png";
        try
        {
            URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+accessToken);
            System.out.println(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");// 提交模式
            // conn.setConnectTimeout(10000);//连接超时 单位毫秒
            // conn.setReadTimeout(2000);//读取超时 单位毫秒
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            paramJson.put("scene", "1234567890");
            paramJson.put("path", "pages/index?query=1");
            paramJson.put("width", 430);
            paramJson.put("is_hyaline", true);
            paramJson.put("auto_color", true);

            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据
            BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());

            String format = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss:SSS").format(new Date());
            String file = p+format+"/"+format+".png";
            OutputStream os = new FileOutputStream(new File(codeUrl));
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1)
            {
                os.write(arr, 0, len);
                os.flush();
            }
            os.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return twoCodeUrl;
    }*/


    /**
     *
     * @param scene 传递参数
     * @param page  二维码跳转页面
     * @param membersAccount 会员id
     * @param access_token
     * @return 二维码文件名
     * @throws Exception
     */
    public static String getCodePic(String scene,String page,String membersAccount,String access_token) throws Exception{
      //  String access_token =null;

       // String =request.getSession().getServletContext().getRealPath("/");

        String code = smallCodeSave;
        File codefile=new File(code);
        if(!codefile.exists() && !codefile.isDirectory()){
            codefile.mkdir();
        }

        File f=new File(smallCodeSave+membersAccount);
        if(!f.exists() && !f.isDirectory()){
            f.mkdir();
        }
        try
        {
            URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+access_token);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");// 提交模式
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            paramJson.put("scene", scene);
            paramJson.put("page", page);
            paramJson.put("width", 430);
            paramJson.put("width", 640);
            paramJson.put("auto_color", true);


            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());
            String fileName = membersAccount+".png";
            OutputStream os = new FileOutputStream(new File(smallCodeSave+membersAccount+"/"+"0.png"));
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1)
            {
                os.write(arr, 0, len);
                os.flush();
            }
            os.close();

            String srcPath = "E:\\var\\data\\iweichuangfu\\images\\code\\"+membersAccount+"\\"+"0.png";
          String newPath = "E:\\var\\data\\iweichuangfu\\images\\code\\"+membersAccount+"/"+fileName;
          coverImage(srcPath,"C:\\Users\\Administrator\\Desktop\\20181105151452.jpg",0,0,350,350,membersAccount,newPath);//二维码中间的那张LOGO底色
            //coverImage(srcPath,srcPath,0,0,300,300,membersAccount,newPath);//二维码中间的那张LOGO底色
            new File(srcPath).delete();
            return membersAccount+"/"+fileName;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static BufferedImage coverImage(String baseFilePath, String coverFilePath, int x, int y, int width, int height, String info, String url) throws Exception{

        BufferedImage destImage = null;
        int allw = 0, allh = 0;

        File baseFile = new File(baseFilePath);//底图
        BufferedImage buffImg = ImageIO.read(baseFile);

        int srcImgWidth = buffImg.getWidth(null);//获取图片的宽
        int srcImgHeight = buffImg.getHeight(null);//获取图片的高

        File coverFile = new File(coverFilePath); //覆盖层
        BufferedImage coverImg = ImageIO.read(coverFile);


         allh = srcImgHeight+coverImg.getHeight(null);

        destImage = new BufferedImage(srcImgWidth, allh, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = coverImg.createGraphics();
        g.setFont(new Font("宋体", Font.BOLD, 35));              //设置字体
        //g.setBackground(new Color(0,0,100));
        g.setColor(new Color(120, 171, 214));

        //设置水印的坐标
        x=0;
        y=0;
        System.out.println("x:"+x);
        System.out.println("y:"+y);
        int strWidth = g.getFontMetrics().stringWidth(info);

        System.out.println("strWidth :"+strWidth );

       // g.drawString(info, 125-strWidth/2, 0);
        g.drawString(info, 320-strWidth/2, 40);
        g.dispose();

        buffImg = coverImage(destImage,buffImg, coverImg, x, y,info);

        FileOutputStream outImgStream = new FileOutputStream(url);
        ImageIO.write(buffImg, "png", outImgStream);
        System.out.println("添加水印完成");
        outImgStream.flush();
        outImgStream.close();


        return buffImg;
    }

    public static BufferedImage coverImage(BufferedImage baseBufferedImage, BufferedImage BufferedImage,BufferedImage coverBufferedImage, int x, int y,String info) throws Exception{

        // 创建Graphics2D对象，用在底图对象上绘图
        Graphics2D g2d = baseBufferedImage.createGraphics();

        // 绘制
      //  g2d.drawImage(coverBufferedImage, 170, 170, 300, 300, null);
        g2d.drawImage(BufferedImage, 0, 0, BufferedImage.getWidth(), BufferedImage.getHeight(), null);
        g2d.drawImage(coverBufferedImage, 0, baseBufferedImage.getHeight()-coverBufferedImage.getHeight(), coverBufferedImage.getWidth(), coverBufferedImage.getHeight(), null);
        g2d.dispose();// 释放图形上下文使用的系统资源
        return baseBufferedImage;
    }
}
