package com.xiao.demo.utils;



import java.util.Date;

public final class OrderUtils {

	public static String createSmsOrderNo(){
		return DateUtils.format(new Date(), "yyyyMMddHHmmssSSS"+ RandomUtils.getRandomDigit(2));
	}
	
	public static String createProductOrderId(){
		return DateUtils.format(new Date(), "yyyyMMddHHmmssSSS");
	}
	public static String createTradeNo(){
		return DateUtils.format(new Date(), "yyyyMMddHHmmssSSS"+ RandomUtils.getRandomDigit(5));
	}
	public static String createLongOrderId(){
		return DateUtils.format(new Date(), "yyyyMMddHHmmssSSS"+ RandomUtils.getRandomDigit(5));
	}
	public static String createOrderId(){
		return DateUtils.format(new Date(), "yyyyMMddHHmmssSS");
	}
}
