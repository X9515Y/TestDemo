package com.xiao.demo.utils;

import java.sql.*;

public final class JDBCUPGtils {
	private static String driver="org.postgresql.Driver";
	//private static String url="jdbc:postgresql://localhost:5432/rongtong?useCursorFetch=true";
	//private static String user="admin";
	//private static String password="123456";

	private static String url="jdbc:postgresql://211.149.157.184:5432/yishengqin?useCursorFetch=true";
	private static String user="postgres";
	private static String password="9MXas1yX0kBuXT00jyqe";

	private JDBCUPGtils(){}

	static {
		/**
		 * 驱动注册
		 */
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new ExceptionInInitializerError(e);
		}

	}

	/**
	 * 获取 Connetion
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		return DriverManager.getConnection(url, user, password);
	}

	/**
	 * 释放资源
	 * @param conn
	 * @param st
	 * @param rs
	 */
	public static void colseResource(Connection conn,Statement st,ResultSet rs) {
		closeResultSet(rs);
		closeStatement(st);
		closeConnection(conn);
	}

	/**
	 * 释放连接 Connection
	 * @param conn
	 */
	public static void closeConnection(Connection conn) {
		if(conn !=null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//等待垃圾回收
		conn = null;
	}

	/**
	 * 释放语句执行者 Statement
	 * @param st
	 */
	public static void closeStatement(Statement st) {
		if(st !=null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//等待垃圾回收
		st = null;
	}

	/**
	 * 释放结果集 ResultSet
	 * @param rs
	 */
	public static void closeResultSet(ResultSet rs) {
		if(rs !=null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//等待垃圾回收
		rs = null;
	}
}