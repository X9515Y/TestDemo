package com.xiao.demo.test;

import java.sql.*;

import com.xiao.demo.utils.JDBCUPGtils;
import org.junit.jupiter.api.Test;
import com.xiao.demo.utils.JDBCUtils;

public class JdbcTest {
	@Test
	public void select() {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		Connection connpg = null;
		PreparedStatement stpg = null;
		ResultSet rspg = null;

		try {
			long start = System.currentTimeMillis();
			// 获取连接
			conn = JDBCUtils.getConnection();
			connpg = JDBCUPGtils.getConnection();

			String findsql ="SELECT member_account,password,member_account,member_account,recommended_man,id_card,service_center,bank_account FROM members";
			//String nideshop_usersql ="SELECT username FROM nideshop_user";
			stpg = connpg.prepareStatement(findsql);
			stpg.setFetchSize(1000);

			rs = stpg.executeQuery();

			/*PreparedStatement preparedStatement = conn.prepareStatement(nideshop_usersql);
			preparedStatement.setFetchSize(1000);
			ResultSet resultSet = preparedStatement.executeQuery();*/

			// 创建语句执行者
			st=conn.prepareStatement("INSERT INTO nideshop_user (username,password,nickname,member_account,recommended_man,id_card,service_center,bank_num) values(?,?,?,?,?,?,?,?)");

			int a =1;
			while(rs.next()) {

				String username = rs.getString("member_account");

				st.setString(1,username);
				st.setString(2,rs.getString("password"));
				st.setString(3,username);
				st.setString(4,username);
				st.setString(5,rs.getString("recommended_man"));
				st.setString(6,rs.getString("id_card"));
				st.setString(7,rs.getString("service_center"));
				st.setString(8,rs.getString("bank_account"));

				st.addBatch();
				//st.addBatch("insert into user(name,contact_member) values('"+member_account+"','"+cantact_man+"')");
				//st.addBatch("INSERT INTO nideshop_user (username,password,nickname,member_account,recommended_man,id_card,service_center,bank_num) values('"+username+"','"+password+"','"+username+"','"+username+"','"+recommended_man+"','"+id_card+"','"+service_center+"','"+bank_num+"')");
				if(a%10000 == 0){
					st.executeBatch();
					st.clearBatch();
				}
				a++;
			}
			st.executeBatch();
			st.clearBatch();
			long end=System.currentTimeMillis();
			System.out.println("一共用了"+(end-start)/1000+"秒");
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtils.colseResource(conn, st, rs);
			JDBCUPGtils.colseResource(connpg, stpg, rspg);
		}

	}
}