package com.xiao.demo.test;

import com.xiao.demo.utils.JDBCUPGtils;
import com.xiao.demo.utils.JDBCUtils;
import com.xiao.demo.utils.QrCodeUtil_new;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcQRcodeTest {
	@Test
	public void select() {

		Connection connpg = null;
		PreparedStatement stpg = null;
		ResultSet rspg = null;

		try {
			long start = System.currentTimeMillis();
			// 获取连接
			connpg = JDBCUPGtils.getConnection();
			//SELECT * from members where activation_time is not null;
			String findsql ="SELECT member_account from members where activation_time is not null";
			//String nideshop_usersql ="SELECT username FROM nideshop_user";
			stpg = connpg.prepareStatement(findsql);
			stpg.setFetchSize(1000);

			rspg = stpg.executeQuery();

			String token = QrCodeUtil_new.getToken();
			int a =1;
			while(rspg.next()) {

				String username = rspg.getString("member_account");
				QrCodeUtil_new.getCodePic(username,"pages/index/index",username,token,"C:\\Users\\Administrator\\Desktop\\qrcode\\");

			}

			long end=System.currentTimeMillis();
			System.out.println("一共用了"+(end-start)/1000+"秒");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUPGtils.colseResource(connpg, stpg, rspg);
		}

	}
}